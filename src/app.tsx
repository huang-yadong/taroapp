import Taro, { Component, Config } from '@tarojs/taro'
import Index from './pages/index'
// import 'taro-ui/dist/style/index.scss' // 引入组件样式 - 方式一
import './app.scss'
// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

class App extends Component {

  componentDidMount () {}

  componentDidShow () {}

  componentDidHide () {}

  componentDidCatchError () {}

  /**
   * 指定config的类型声明为: Taro.Config
   *  到底为止, 升级到taro3.3.20
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    pages: [
      'pages/home/home',
      'pages/index/index',
      'pages/me/me'
    ],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'huangzhuang',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      position: 'bottom',
      list: [{pagePath:'pages/home/home', text: '壁纸'},
        {pagePath:'pages/index/index', text: '图片'},
        {pagePath:'pages/me/me', text: '我的'}]
    }
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render () {
    return (
      <Index />
    )
  }
}

Taro.render(<App />, document.getElementById('app'))
