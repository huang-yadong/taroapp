import {View,  ScrollView, Image} from "@tarojs/components";
import './home.scss'
import {AtPagination} from "taro-ui";
import '../js/gloabl'
export default class PageView extends Taro.Component<any,any> {

    constructor(props) {
        super(props);
        this.state = {
            imageList: ['https://res.wx.qq.com/wxdoc/dist/assets/img/0.4cb08bb4.jpg'],
            total: 2,
            size: 1,
            current: 1
        }
        console.log(global.constants.webroot);
        //this.updateImage = this.updateImage.bind(this);
        //this.tick = this.tick.bind(this);
    }

    componentDidMount () {
        console.log("xuan然完毕")
       /* console.log("xuan然完毕")
        const pageData = {
            current: this.state.current,
            limit: this.state.pageSize
        }
        this.request(pageData);*/
        // const pageData = {
        //     current: this.state.current,
        //     limit: this.state.size
        // }
        // console.log("constructor" + this.state.total);
        // this.request(pageData);
        // console.log("constructor end" + this.state.total)
    }

    /**
     * current: this.state.current,
     * pageSize: this.state.pageSize
     */
     requestMethod(pageData){Taro.request({
            url: global.constants.webroot + '/image/page/images', //仅为示例，并非真实的接口地址
            method: 'GET',
            data: {
                current: pageData.current,
                limit: this.state.size
            },
            header: {
                'content-type': 'application/json' // 默认值
            },
            success: function (response) {
                console.log(response.data);
                const result = response.data;
                let images = new Array();
                for (let item of result.records) {
                    images.push(item.imageUrl)
                }
                console.log(result.total);
                this.setState({
                    imageList: images,
                    current: pageData.current,
                    size: result.size,
                    total: result.total
                }, () => {
                    console.log("aa");
                });
                console.log(this.state);
            }
        })

    }

    // 异步访问
/*
        const caa = new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open('GET','http://localhost:8080/pro/test');
            //xhr.setRequestHeader('content-type','application/json;charset=UTF-8');
            xhr.setRequestHeader('content-type','text/xml');
            xhr.send();
            xhr.onreadystatechange=function () {
                console.log("xhr state" + xhr.readyState);// 准备状态2不能拿到response  3  4 能拿到response
                console.log("xhr state" + xhr.status); // http 状态码
                console.log("xhr state" + xhr.statusText);
                console.log("response" + xhr.response);
                console.log("response" + xhr.responseText);
                resolve(xhr.response);
                reject(2);
            }

        });
        caa.then(value1 => {
            console.log("resolve" + value1);
        },reason => {
            console.log("reason" + reason);
        });*/

    render() {
        const scrollStyle = {

        }

        return (

            <ScrollView className='components-page'
                scrollY={true}
                scrollWithAnimation
                scrollTop={20}
                style={scrollStyle}
            >
                <View style={'heiht: 219px'}>
                    {
                        this.state.imageList.map(
                            (e) => {
                                return (
                                    <Image
                                        style='width: 48%;height: 219px;background: #fff;'
                                        src={e}
                                    />
                                )
                            }
                        )
                    }
                </View>
                <AtPagination className='page'
                              total={this.state.total}
                              pageSize={this.state.size}
                              current={this.state.current}
                              onPageChange={pageData => {
                                  this.requestMethod(pageData);
                              } }
                >
                </AtPagination>
            </ScrollView>

        )
    }
}
