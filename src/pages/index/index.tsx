import { View, Text } from '@tarojs/components'
import { AtButton } from 'taro-ui'
import './index.scss'
import Config = Taro.Config;

export default class Index extends Taro.Component {

  componentWillMount () { }

  componentDidMount () { }

  componentWillUnmount () { }

  componentDidShow () { }

  componentDidHide () { }

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '首页'
  }

  /*render () {
    return (
      <View className='index'>
        <Text>Hello world!</Text>
      </View>
    )
  }*/
  render () {
    return (

        <View className='index'>
          <Text>Hello world!</Text>
          <AtButton type='primary'>按钮文案</AtButton>
        </View>
    )
  }
}
