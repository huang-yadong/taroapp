import './me.scss'
import { AtButton } from 'taro-ui'
export default class PageView extends Taro.Component {
    constructor() {
        super(...arguments)
    }

    render() {
        return (
            <view>
                <AtButton>按钮文案</AtButton>
                <AtButton type='primary' onClick={() => {
                    //wx.login()
                }}>按钮文案</AtButton>
                <AtButton loading type='primary'>按钮文案</AtButton>
            </view>
        )
    }
}
